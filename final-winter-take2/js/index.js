var image_cell_template = "<div class='col-sm-3 image-cell'><div class='nasa-image'><img></div><div class='image-caption'></div><div class='image-coords'></div></div>";
var date_tmpl = "begin=YEAR-MONTH-DAY";
var myApiKey = "?api_key=Wa6vQZ59Ja1o5SwN1Gse98NwHq0mmH8lbfa0pQQJ";

var example_image_url = "https://api.nasa.gov/EPIC/archive/natural/2017/08/21/png/epic_1b_20150613110250_01.png?api_key=Wa6vQZ59Ja1o5SwN1Gse98NwHq0mmH8lbfa0pQQJ";
var epic_natural_archive_base = "https://api.nasa.gov/EPIC/archive/natural/";
var api_url_query_base = "https://epic.gsfc.nasa.gov/api/natural/date/";

// ==========================================================
// START JS: synchronize the javascript to the DOM loading
// ==========================================================
$(document).ready(function() {

  // ========================================================
  // SECTION 1:  process on click events
  // ========================================================
  $('#get-images-btn').on('click', api_search);

  // process the "future" img element dynamically generated
  $("div").on("click", "img", function(){
    console.log(this.src);
    // call render_highres() and display the high resolution
  });

  // ========================================================
  // TASK 1:  build the search AJAX call on NASA EPIC
  // ========================================================
  // Do the actual search in this function
  function api_search(e) {

    // get the value of the input search text box => date
    var date =  $('#search_date').val();

    // build an info object to hold the search term and API key
    var info = {};
    var date_array = date.split('-');
    console.log(date_array);
    info.year = date_array[0];
    info.month = date_array[1];
    info.day = date_array[2];
    info.api_key = "Wa6vQZ59Ja1o5SwN1Gse98NwHq0mmH8lbfa0pQQJ"

    // build the search url and sling it into the URL request HTML element
    var search_url = api_url_query_base + info.year + "-" + info.month +  "-" + info.day +  myApiKey;
    console.log(search_url);
    // sling it!

    // make the jQuery AJAX call!
    $.ajax({
      dataType: "jsonp",
      url: search_url,
      success: function(data) {
       /* for (i=0; i<data.length; i++) {
        var data1 = data[i]['image'];
          $('#reqURL').innerHTML = search_url;
          console.log(data1);*/
        render_images(data,info);
        
      },
      cache: false
    });
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================

  // ========================================================
  // TASK 2: perform all image grid rendering operations
  // ========================================================
  function render_images(data,info) {
    // get NASA earth data from search results data
    var earth = epic_natural_archive_base + info.year + "-" + info.month + "-" + info.day + "-" + myApiKey;
    console.log(data.length);
    var images = [];
    for (var i = 0; i < data.length; i++) {
      var image;
      // build an array of objects that captures all of the key image data
      // => image url
      //images.push(data["date"]["image"][i]);
      image.url = "https://api.nasa.gov/EPIC/archive/natural/" + info.year + "-" + info.month + "-" + info.day + "-" + data[i].image + myApiKey;

      
      // => centroid coordinates to be displayed in the caption area
      image.coordinates = "Latitude:" + data[i].centroid_coordinates.lat + ", Longitude:" +data[i].centroid_coordinates.lon;
      
      // => image date to be displayed in the caption area (under thumbnail)
      image.caption = data[i].date;
      images[i] = image;
      /*$("#image-grid").append("<img src='https://api.nasa.gov/EPIC/archive/natural/2017/08/21/png/epic_1b_20150613110250_01.png?api_key=Wa6vQZ59Ja1o5SwN1Gse98NwHq0mmH8lbfa0pQQJ'/>");
      console.log(images);  
      )*/
    

    }


    // select the image grid and clear out the previous render (if any)
    var earth_dom = $('#image-grid');
    earth_dom.empty();
    for (var i = 0; i < images.length; i++){
      var imageGet = "<div class='col-sm-3 image-cell'><div class='nasa-image'><img src=" + images[i].url + "></div><div class='image-caption'>" + images[i].coordinates + "</div><div class='image-coords'>" + images[i].caption + "</div></div>";
      earth_dom.append(imageGet);}
    // render all images in an iterative loop here!
  }

  // ========================================================
  // TASK 3: perform single high resolution rendering
  // ========================================================
  // function to render the high resolution image
  function render_highres(src_url) {
    // use jQuery to select and maniupate the portion of the DOM => #image-grid
    //  to insert your high resolution image
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
});